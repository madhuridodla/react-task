import axios from "axios";
import { API_URL } from "../configs/configs";

import { getItem } from "../utlis";

/*******************************************************************************************
     @PURPOSE      	: 	Call api.
     @Parameters 	: 	{
       url : <url of api>
       data : <data object (JSON)>
       method : String (get, post)
       isForm (Optional) : Boolean - to call api with form data
       isPublic (Optional) : Boolean - to call api without auth header
     }
     /*****************************************************************************************/
export const commonApiCall = (
  url,
  data,
  method,
  Type = null,
  isAuthorized = false,
  isForm = false,
) => (dispatch) => {
  url = API_URL + url;
  let headers = { "Content-Type": "application/json" },
    token = getItem("token") ? getItem("token") : null;
  if (isAuthorized) headers.Authorization = token;
  if (isForm) headers["Content-Type"] = "multipart/form-data";
  // Api calling
  return new Promise((resolve, reject) => {
    axios({ method, url, headers, data })
      .then((response) => {
        // console.log('response', response)
        if (response && response.status === 200) {
          if (Type !== null) {
            var payload = { data: response.data };
            dispatch({ type: Type, payload });
          }
          let Data = { data: response.data, status: response.status };
          resolve(Data);
        } else if (response && response.status === 404) {
          let Data = { data: response.data, status: response.status };
          resolve(Data);
        } else if (response && response.status === 400) {
          let Data = { data: response.data, status: response.status };
          resolve(Data);
        }
      })
      .catch((error) => {
        console.log("error is ", error);
        reject(error);
      });
  });
};


// Active lead packages
export const GetDetails = (data) => async (dispatch) => {
  dispatch({ type: "GETDETAILS", payload: data });
};
