import React from "react";
import { toast } from "react-toastify";
import errorMessages from '../redux/actions/validationMessages';
toast.configure();

// /**********************************************************************
//  @PURPOSE : We can use following function to store data in localstorage
// //Validations and
// //Messages
/*************************************************************************************
                            Set success Toast Message
  **************************************************************************************/
export const showSuccessToast = (msg) => {
  toast.success(msg, {
    position: "top-right",
    autoClose: 3000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
  });
};
/*************************************************************************************/

/*************************************************************************************
                            Set Error Toast Message
  **************************************************************************************/
export const showErrorToast = (msg) => {
  toast.error(msg, {
    position: "top-right",
    autoClose: 4000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
  });
};
/*************************************************************************************/

/*************************************************************************************
                            Set WARNING Toast Message
  **************************************************************************************/
export const showWarnToast = (msg) => {
  toast.warn(msg, {
    position: "top-right",
    autoClose: 4000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
  });
};
/*************************************************************************************/
//   /*************************************************************************************
//                              Get Data From local Storage
//  **************************************************************************************/
export const getItem = (key) => {
  return localStorage.getItem(key);
};