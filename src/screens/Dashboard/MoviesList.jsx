import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import { commonApiCall, GetDetails } from '../../utlis'
import Pagination from "react-js-pagination";

function EmployeesList({ commonApiCall, GetDetails, ...props }) {

  // variable declaration 
  const [moviesArray, setMoviesArray] = useState([])
  const [page, setPage] = useState({
    totalItemsCount: 1,
    activePage: 1,
    pageRangeDisplayed: null,
    itemsCountPerPage: null,
  });

  useEffect(() => {
    GetMoviesList()
  }, []);
  //***********  get movies list************************//
  const GetMoviesList = async (page) => {
    const pageNo = page ? page : 1;
    const response = await commonApiCall('/movie?api_key=f6d6593aaa18fb741d0686beb6f4a2d7&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=' + pageNo, '', 'get', null);
    setMoviesArray([...moviesArray, ...response.data.results])

    setPage({
      ...page,
      activePage: response.data.page,
      totalItemsCount: response.data.total_results,
      itemsCountPerPage: response.data.total_pages,
    });
  }
  //***************************************************//
  //*********** PAGINATION ************************//
  const handlePageChange = (pageNumber) => {
    GetMoviesList(pageNumber);
    setPage({ ...page, activePage: pageNumber });
  };

  /// get details
  const getDetails = (data) => {
    GetDetails(data)
    props.history.push('/movieDetails')
  }
  return (
    <div className="container">
      <h2>Movies List</h2>
      <table>
        <thead>
          <tr>
            <th>S.NO</th>
            <th>Movie Title</th>
            <th>Overview</th>
            <th>Vote Average</th>
            <th>Popularity</th>
            <th>Vote Count</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {
            moviesArray && moviesArray.length && moviesArray.map((data, key) => {
              return (
                <tr key={key} onClick={() => getDetails(data)}>
                  <td>{key + 1}</td>
                  <td>{data.title}</td>
                  <td>{data.overview}</td>
                  <td>{data.vote_average}</td>
                  <td>{data.popularity}</td>
                  <td>{data.vote_count}</td>
                  <td onClick={() => getDetails(data)}> <i className="fa fa-eye" aria-hidden="true"></i></td>
                </tr>
              )
            })
          }
        </tbody>

      </table>

      {moviesArray.length ? (
        <div className="pagination-block">
          <Pagination
            activePage={page.activePage}
            itemsCountPerPage={page.itemsCountPerPage}
            totalItemsCount={page.totalItemsCount}
            pageRangeDisplayed={3}
            onChange={(e) => handlePageChange(e)}
          />
        </div>
      ) : null}

    </div>
  )
}
export default connect(null, { commonApiCall, GetDetails })(EmployeesList)

