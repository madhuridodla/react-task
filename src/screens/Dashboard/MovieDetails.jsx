import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";
import { connect } from "react-redux";

function EmployeesList({ movie_details, ...props }) {
  // variable declaration 
  const [moviedetails, setMoviedetails] = useState({})
  useEffect(() => {
    setMoviedetails(movie_details)
  }, [movie_details]);

  return (
    <div className="container">
      <div className="login-wrapper">
        <div className="title-header">
          <h2>Movie Details</h2>
          <Link to="" className="btn btn-link" title="Back" onClick={() => props.history.push('/')} > Back </Link>
        </div>
        <div className="row">
          <div className="col-md-12">
            <label htmlFor="title" className="text">Movie Title :</label>  {moviedetails.title}
          </div>
          <div className="col-md-12">
            <label htmlFor="title" className="text">Overview :</label>  {moviedetails.overview}
          </div>
          <div className="col-md-12">
            <label htmlFor="title" className="text">Vote Average :</label>  {moviedetails.vote_average}
          </div>
          <div className="col-md-12">
            <label htmlFor="title" className="text">Popularity :</label>  {moviedetails.popularity}
          </div>
          <div className="col-md-12">
            <label htmlFor="title" className="text">Vote Count :</label>  {moviedetails.vote_count}
          </div>

        </div>

      </div>
    </div>
  )
}
const mapStateToProps = (state) => {
  return {
    movie_details: state.reducer.movieDetails,
  };
};
export default connect(mapStateToProps, null)(EmployeesList);

