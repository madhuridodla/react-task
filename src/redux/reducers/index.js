import { combineReducers } from 'redux';
import MoviesReducer from './MoviesReducer';

// Combine all reducers into root reducer
export default combineReducers({
    reducer: MoviesReducer
});