import { LOGIN } from '../actions'
const INTIAL_STATE = {
    login: {},
    movieDetails: {}
}
export default (state = INTIAL_STATE, action) => {
    switch (action.type) {
        case LOGIN:
            return Object.assign({}, state, { login: action.payload.data })
        case "GETDETAILS":
            return Object.assign({}, state, { movieDetails: action.payload })

        case "LOGOUT":
            return INTIAL_STATE
        default:
            return state;
    }

}