import React, { lazy, Suspense } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { DashboardPageRoutes } from './utlis'
const MoviesList = lazy(() => import("./screens/Dashboard/MoviesList"));
const MovieDetails = lazy(() => import("./screens/Dashboard/MovieDetails"));


function AppRouting() {
  return (
    <React.Fragment>
      <BrowserRouter>
        <Switch>
          <Suspense fallback={<div></div>}>
            <Route
              exact
              path={DashboardPageRoutes.MOVIES_LIST}
              component={MoviesList}
            />
            <Route
              exact
              path={DashboardPageRoutes.MOVIES_DETAILS}
              component={MovieDetails}
            />
          </Suspense>
        </Switch>
      </BrowserRouter>
    </React.Fragment>
  )
}
export default AppRouting