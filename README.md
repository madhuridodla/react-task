# mmr-omo-defy-frontend

## Prequisites (Development):

| Module | Version |
| ------ | ------- |
| Node   | 12.14.0 |
| Npm    | 6.13.4  |
| React  | 16.13.1 |
| Redux  | 4.0.5   |

---

##### Take Clone of project

> \$ git clone -b branch_name git_url folder_name

##### Development Process

> \$ npm install

> \$ npm start

---

##### Deployment Process

> \$ npm run build

---
